<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;

class MainController extends Controller
{
    public function index()
    {
    	return view('main.home');
    }

    public function about()
    {
    	return view('main.about');
    }

    public function contact( )
    {
    	return view('main.contact');
    }

    public function shop( )
    {
    	return view('main.shop');
    }
}
