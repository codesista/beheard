<?php
Route::get('/','MainController@index');
Route::get('/contact','MainController@contact');
Route::get('/submit','MainController@submit');
Route::get('/about','MainController@about');
Route::get('/shop','MainController@shop');
